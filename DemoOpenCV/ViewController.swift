//
//  ViewController.swift
//  DemoOpenCV
//
//  Created by 陳鈞廷 on 2017/6/3.
//  Copyright © 2017年 陳鈞廷. All rights reserved.
//

import UIKit

class ViewController: UIViewController {


    @IBOutlet weak var before: UIImageView!
    @IBOutlet weak var after: UIImageView!
    let img = UIImage(named: "paper(1).JPG")
    var count = 0;
    @IBAction func ImageProcessing(_ sender: UIButton) {
        switch count {
        case 0:
            after.image = OpenCVWrapper.grayScale(img)
            count += 1
        case 1:
            after.image = OpenCVWrapper.cannyEdge(img)
            count += 1
        case 2:
            after.image = OpenCVWrapper.drawAllContourWithoutConvex(img)
            count += 1
        case 3:
            after.image = OpenCVWrapper.drawAllContour(img)
            count += 1
        case 4:
            after.image = OpenCVWrapper.drawMaxContour(img)
            count += 1
        case 5:
            after.image = OpenCVWrapper.doMyImageProcessing(img)
            count += 1
        default:
            after.image = img
            count = 0
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        before.image = img
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}

