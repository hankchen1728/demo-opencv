//
//  OpenCVWrapper.h
//  DemoOpenCV
//
//  Created by 陳鈞廷 on 2017/6/3.
//  Copyright © 2017年 陳鈞廷. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface OpenCVWrapper : NSObject
//get opencv version
+ (NSString *) openCVVersionString;
//function to get edged
#ifdef __cplusplus
#include <opencv2/opencv.hpp>
+ (void) FixOrientation:(UIImage *)image andMat:(cv::Mat &)imageMat;
+ (void) FindMaxContour:(cv::Mat &)imageMat andcnt:(std::vector<cv::Point> &)maxContour;
+ (void) MatProcessingToMat:(cv::Mat &)imageMat andimage:(cv::Mat &)processImage;
#endif
+ (UIImage *) GrayScale:(UIImage *)image;
+ (UIImage *) CannyEdge:(UIImage *)image;
+ (UIImage *) DrawAllContourWithoutConvex:(UIImage *)image;
+ (UIImage *) DrawAllContour:(UIImage *)image;
+ (UIImage *) DrawMaxContour:(UIImage *)image;
+ (int) ContourCondition:(UIImage *)image;
+ (UIImage *) DoMyImageProcessing:(UIImage *)image;

@end
