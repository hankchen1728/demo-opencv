//
//  OpenCVWrapper.m
//  DemoOpenCV
//
//  Created by 陳鈞廷 on 2017/6/3.
//  Copyright © 2017年 陳鈞廷. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include <math.h>
#import <opencv2/opencv.hpp>
#import "OpenCVWrapper.h"
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/videoio/cap_ios.h>
#include <opencv2/core/core.hpp>
using namespace std ;

@implementation OpenCVWrapper

// here we can use C++ function

+(NSString *) openCVVersionString
{
    return [NSString stringWithFormat:@"OpenCV Version %s",CV_VERSION];
}
+(void) FixOrientation:(UIImage *)image andMat:(cv::Mat &)imageMat{
    //maintain orientation of image
    if (image.imageOrientation == UIImageOrientationUp){
        UIImageToMat(image, imageMat);
    }
    else{
        UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
        [image drawInRect:(CGRect){0, 0, image.size}];
        UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        UIImageToMat(normalizedImage, imageMat);
    }
    return;
}
+(UIImage *) GrayScale:(UIImage *)image{
    cv::Mat imageMat;
    [OpenCVWrapper FixOrientation:image andMat:imageMat];
    int row = imageMat.rows;
    int col = imageMat.cols;
    cv::resize(imageMat, imageMat, cv::Size(int(col*500/row),500));
    cv::Mat grayMat;
    cv::cvtColor(imageMat, grayMat, CV_BGR2GRAY);
    return MatToUIImage(grayMat);
}
+(UIImage *) CannyEdge:(UIImage *)image{
    image = [OpenCVWrapper GrayScale:image];
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    cv::Mat grayMat;
    cv::GaussianBlur(imageMat, grayMat, cvSize(5,5), 0);
    cv::Mat cannyMat;
    cv::Canny(grayMat, cannyMat, 75, 200);
    return MatToUIImage(cannyMat);
}
+(UIImage *) DrawAllContourWithoutConvex:(UIImage *)image{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    int row = imageMat.rows;
    int col = imageMat.cols;
    cv::resize(imageMat, imageMat, cv::Size(int(col*500/row),500));
    UIImage *cannyImage = [OpenCVWrapper CannyEdge:image];
    cv::Mat cannyMat;
    UIImageToMat(cannyImage, cannyMat);
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(cannyMat, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
    CvScalar color = cvScalar(255,0,0,255); //(r,g,b,飽和值)
    for (int i = 0; i<contours.size(); i++) {
        std::vector<cv::Point> cnt = contours[i];
        //double area = cv::contourArea(contours[i]);
        cv::drawContours(imageMat, contours, i, color,5);
    }
    return MatToUIImage(imageMat);
}
+(UIImage *) DrawAllContour:(UIImage *)image{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    int row = imageMat.rows;
    int col = imageMat.cols;
    cv::resize(imageMat, imageMat, cv::Size(int(col*500/row),500));
    UIImage *cannyImage = [OpenCVWrapper CannyEdge:image];
    cv::Mat cannyMat;
    UIImageToMat(cannyImage, cannyMat);
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(cannyMat, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
    CvScalar color = cvScalar(255,0,0,255); //(r,g,b,飽和值)
    for (int i = 0; i<contours.size(); i++) {
        std::vector<cv::Point> cnt = contours[i];
        cv::convexHull(cnt, contours[i],false);
        //double area = cv::contourArea(contours[i]);
        cv::drawContours(imageMat, contours, i, color,5);
    }
    return MatToUIImage(imageMat);
}
+(UIImage *) DrawMaxContour:(UIImage *)image{
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    int row = imageMat.rows;
    int col = imageMat.cols;
    cv::resize(imageMat, imageMat, cv::Size(int(col*500/row),500));
    UIImage *cannyImage = [OpenCVWrapper CannyEdge:image];
    cv::Mat cannyMat;
    UIImageToMat(cannyImage, cannyMat);
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(cannyMat, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
    CvScalar color = cvScalar(255,0,0,255); //(r,g,b,飽和值)
    
    double maxarea = 0;
    std::vector<std::vector<cv::Point>> maxcnt = {{}};
    
    int MaxCntIndex = 0;
    //CvScalar color = cvScalar(255,0,0,255); //(r,g,b,飽和值)
    
    for (int i = 0; i<contours.size(); i++) {
        std::vector<cv::Point> cnt = contours[i];
        cv::convexHull(cnt, contours[i],false);
        double area = cv::contourArea(contours[i]);
        if (area > maxarea){
            maxarea = area;
            maxcnt[0] = contours[i];
            MaxCntIndex = i;
        }
    }
    cv::drawContours(imageMat, maxcnt, 0, color, 5);
    return MatToUIImage(imageMat);
}
+(void) FindMaxContour:(cv::Mat &)imageMat andcnt:(std::vector<cv::Point> &)maxContour{
    int row = imageMat.rows;
    int col = imageMat.cols;
    
    cv::Mat SmallImageMat;
    //resize
    cv::resize(imageMat, SmallImageMat, cv::Size(int(col*500/row),500));
    //transform image to gray
    cv::Mat grayMat;
    cv::cvtColor(SmallImageMat, grayMat, CV_BGR2GRAY);
    
    cv::GaussianBlur(grayMat, grayMat, cvSize(5,5), 0);
    
    //canny edge detect
    cv::Mat cannyMat;
    cv::Canny(grayMat, cannyMat, 75, 200);
    
    //find contours and draw contours
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(cannyMat, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
    double maxarea = 0;
    std::vector<std::vector<cv::Point>> maxcnt = {{}};
    
    int MaxCntIndex = 0;
    //CvScalar color = cvScalar(255,0,0,255); //(r,g,b,飽和值)
    
    for (int i = 0; i<contours.size(); i++) {
        std::vector<cv::Point> cnt = contours[i];
        cv::convexHull(cnt, contours[i],false);
        double area = cv::contourArea(contours[i]);
        if (area > maxarea){
            maxarea = area;
            maxcnt[0] = contours[i];
            MaxCntIndex = i;
        }
    }

    cv::approxPolyDP(maxcnt[0], maxcnt[0], 0.01*cv::arcLength(maxcnt[0], true), true);
    cout << maxcnt[0];
    maxContour = maxcnt[0];
}
+(int) ContourCondition:(UIImage *)image{
    //transform UIImage to cv::Mat
    cv::Mat imageMat;
    [OpenCVWrapper FixOrientation:image andMat:imageMat];

    int contourCase = 0;
    int row = imageMat.rows;
    int col = imageMat.cols;
    
    std::vector<cv::Point> contour;
    [OpenCVWrapper FindMaxContour:imageMat andcnt:contour];
    int smallRow = 500;
    int smallCol = int(col*500/row);
    int GonCount = static_cast<int>(contour.size());
    std::vector<int> Xpts(GonCount);
    std::vector<int> Ypts(GonCount);
    for(int i=0;i<GonCount;i++){
        Xpts[i] = contour[i].x;
        Ypts[i] = contour[i].y;
    }
    
    if ((find(Xpts.begin(), Xpts.end(), 0)!=Xpts.end() && find(Xpts.begin(), Xpts.end(), smallCol-1)!=Xpts.end())||
        (find(Ypts.begin(), Ypts.end(), 0)!=Ypts.end() && find(Ypts.begin(), Ypts.end(), smallRow-1)!=Ypts.end())){
        contourCase = 1; //請將手機往上抬
    }
    else if (find(Xpts.begin(), Xpts.end(), 0)!=Xpts.end()){
        contourCase = 2; //請將手機往左移
    }
    else if (find(Xpts.begin(), Xpts.end(), smallCol-1)!=Xpts.end()){
        contourCase = 3; //請將手機往右移
    }
    else if (find(Ypts.begin(), Ypts.end(), 0)!=Ypts.end()){
        contourCase = 4; //請將手機往前移動
    }
    else if (find(Ypts.begin(), Ypts.end(), smallRow-1)!=Ypts.end()){
        contourCase = 4; //請將手機往後移動
    }
    return contourCase;
}
+(void) MatProcessingToMat:(cv::Mat &)imageMat andimage:(cv::Mat &)processImage{
    int row = imageMat.rows;
    int col = imageMat.cols;
    double ratio = row/500.0;
    
    std::vector<cv::Point> maxcnt;
    [OpenCVWrapper FindMaxContour:imageMat andcnt:maxcnt];

    //check whether the contour is a quadrilateral
    if (maxcnt.size()==4){
        //order point
        cv::Point2f origin = cv::Point2f(0,0);
        std::vector<cv::Point2f> pts ={origin,origin,origin,origin};
        for (int i=0;i<4;i++){
            pts[i].x = maxcnt[i].x*ratio;
            pts[i].y = maxcnt[i].y*ratio;
        }
        cv::Point2f TopLeft,TopRight,BottomRight,BottomLeft;
        std::vector<int> sum ={0,0,0,0};
        for(int i=0;i<4;i++){
            sum[i] = int(pts[i].x+pts[i].y);
        }
        auto result = std::minmax_element(sum.begin(), sum.end());
        TopLeft = pts[result.first-sum.begin()];
        BottomRight = pts[result.second-sum.begin()];
        std::vector<int> diff ={0,0,0,0};
        for(int i=0;i<4;i++){
            diff[i] = int(pts[i].y-pts[i].x);
        }
        result = std::minmax_element(diff.begin(), diff.end());
        TopRight = pts[result.first-diff.begin()];
        BottomLeft = pts[result.second-diff.begin()];
        //start to transform four points
        double widthA = sqrt(pow(double(BottomRight.x - BottomLeft.x),2.0) + pow(double(BottomRight.y - BottomLeft.y),2.0));
        double widthB = sqrt(pow(double(TopRight.x - TopLeft.x),2.0) + pow(double(TopRight.y - TopLeft.y),2.0));
        double maxwidth = std::max(widthA, widthB);
        double heightA = sqrt(pow(double(TopRight.x - BottomRight.x),2.0) + pow(double(TopRight.y - BottomRight.y),2.0));
        double heightB = sqrt(pow(double(TopLeft.x - BottomLeft.x),2.0) + pow(double(TopLeft.y - BottomLeft.y),2.0));
        double maxheight = std::max(heightA, heightB);
        std::vector<cv::Point2f> points = {cv::Point2f(0.0, 0.0),
            cv::Point2f(maxwidth - 1.0, 0.0),
            cv::Point2f(maxwidth - 1.0, maxheight - 1.0),
            cv::Point2f(0.0, maxheight - 1.0)};
        std::vector<cv::Point2f> rect = {TopLeft,TopRight,BottomRight,BottomLeft};
        
        cv::Mat matrix = cv::getPerspectiveTransform(rect, points);
        cv::Mat warped;
        cv::warpPerspective(imageMat, warped, matrix, cv::Size(maxwidth,maxheight));
        processImage = warped;
        return;
    }
    //transform cv::Mat to UIImage and then return UIImage
    processImage = imageMat;
    return;
}

+(UIImage *) DoMyImageProcessing:(UIImage *)image{
    //transform UIImage to cv::Mat
    cv::Mat imageMat;
    //maintain orientation of image
    [OpenCVWrapper FixOrientation:image andMat:imageMat];
    
    cv::Mat afterMat;
    //transform cv::Mat to UIImage and then return UIImage
    [OpenCVWrapper MatProcessingToMat:imageMat andimage:afterMat];
    return MatToUIImage(afterMat);
}
@end

